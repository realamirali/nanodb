# NanoDB

NanoDB is lightweight, fast, and simple key/value store based on Python's own  
json module. And it's BSD licensed!

## Use NanoDB
```python
>>> import nanodb

>>> db = nanodb.load('test.db', False)

>>> db.set('key', 'value')  

>>> db.get('key')  
'value'

>>> db.dump()  
True
```

## Install NanoDB
```
$ pip install simplejson
$ git clone https://gitlab.com/AmiraliNull/nanodb.git
$ cd nanodb
$ python setup.py install
```

