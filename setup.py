"""
NanoDB
--------

NanoDB is lightweight, fast, and simple key/value store based on Python's own
json module. And it's BSD licensed!

NanoDB is Fun
```````````````

::

    >>> import nanodb

    >>> db = nanodb.load('test.db', False)

    >>> db.set('key', 'value')

    >>> db.get('key')
    'value'

    >>> db.dump()
    True


And Easy to Install
```````````````````

::

    $ pip install nanodb

Links
`````

* `website <https://amiralinull.github.io/nanodb/>`_
* `documentation <https://amiralinull.github.io/nanodb/doc/>`_
* `project repo <https://github.com/amiralinull/nanodb/>`_

"""

from distutils.core import setup

setup(name = "nanoDB",
    version="0.0.1",
    description="A lightweight and simple key value store.",
    author="Amirali Esfandiari",
    author_email="amiralinull+nanodb@gmail.com",
    license="three-clause BSD",
    url="https://amiralinull.github.io/nanodb/",
    long_description=__doc__,
    classifiers = [
        "Programming Language :: Python",
        "License :: OSI Approved :: BSD License",
        "Intended Audience :: Developers",
        "Topic :: Key/Value Store" ],
    py_modules=['nanodb'],
    install_requires=['simplejson'])
